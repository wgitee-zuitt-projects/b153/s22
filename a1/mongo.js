//eCommerce Data Model for Sports Equipment

//Users 

/* 
    _id: <ObjectId>,
    fname: string, 
    lname: string,
    password: string,
    email: string,
    emailVerified: boolean,
    address: 
        {country: string,
                 street1: string,
                 street2: string,
                 city: string,
                 state: string,
                 zip: string} 
        
*/


//Products

/* _id: <ObjectId>, 
    item: string, 
    features: string,
    skus: 
        {sku: string,
                price: {
                    base: NumberDecimal,
                    currency: string
                    discount: NumberDecimal
                    }
                    quantity: number,
                    options:{
                        size: number,
                        features: string,
                        colors: string,
                        image: string
                    }    
                }           
*/


//====================================================================================

    // "customers",
   
      /*properties: {
         _id: {
            bsonType: string,
            description: string
         },
         Password: {
            bsonType: string,
            minLength: number,
            description: string
         },
         address: {
            bsonType: object,
            required: [ "street1", "zip" ],
            properties: {
                zip: { 
                    description: string
                },
                street1: { 
                    bsonType: "string",
                    description: string,
                street2: { 
                    bsonType: "string",
                    description: string
            },
            description: string
         }
      }*/

//====================================================================================

    //Orders

    /*customerId: string,
        paymentId: string,
        paymentStatus: string,
        status: string,
        currency: string,
        totalCost: NumberDecimal
        items: [ 
            { sku: string,
                quantity: number,
                price: NumberDecimal,
                discount: NumberDecimal,
                preTaxTotal: NumberDecimal,
                tax: NumberDecimal,
                total: NumberDecimal,
            }, 
        ],
        shipping:  {
            address:{
                street1: string,
                street2: string,
                city: string,
                state: string,
                country: string,
                zip: string
            },
        
            carrier: string,
            tracking: number
        }*/
